. "$HOME/.cargo/env"

export PATH="/home/ricsi/.local/share/solana/install/active_release/bin:$PATH"

export NVM_DIR="$HOME/.config/nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
