
"   ████████ ██                               ██    
"  ██░░░░░░ ░██                              ░██    
" ░██       ░██      ██████  ██████  ███████ ░██  ██
" ░█████████░██████ ░░██░░█ ██░░░░██░░██░░░██░██ ██ 
" ░░░░░░░░██░██░░░██ ░██ ░ ░██   ░██ ░██  ░██░████  
"        ░██░██  ░██ ░██   ░██   ░██ ░██  ░██░██░██ 
"  ████████ ░██  ░██░███   ░░██████  ███  ░██░██░░██
" ░░░░░░░░  ░░   ░░ ░░░     ░░░░░░  ░░░   ░░ ░░  ░░

set nocompatible              " be iMproved, required
filetype off                  " required
set backspace=indent,eol,start
set ruler
set nu rnu
set showcmd
set incsearch
set hlsearch
set tabstop=4
set ts=4 sw=4

syntax on

filetype plugin indent on

" Line number coloring
hi LineNrAbove ctermfg=red
hi LineNrBelow ctermfg=green
hi LineNr ctermfg=grey
"hi CursorLineNr=red

" Search result highlight
hi MatchParen cterm=none ctermbg=magenta ctermfg=white

" set the runtime path to include Vundle and initialize
set rtp+=~/.config/nvim/bundle/Vundle.vim
call vundle#begin("~/.config/nvim/bundle")

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'itchyny/lightline.vim'
Plugin 'frazrepo/vim-rainbow'
Plugin 'rust-lang/rust.vim'
Plugin 'rust-lang/rls'
Plugin 'neovim/nvim-lspconfig'
Plugin 'nvim-lua/lsp_extensions.nvim'
Plugin 'nvim-lua/completion-nvim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

map <C-n> :NERDTreeToggle<CR>

set laststatus=2
set t_Co=256

:let g:colorizer_auto_color = 1
:let g:colorizer_syntax     = 1
:let g:colorizer_colornames = 0
:let g:rainbow_active       = 1

let g:lightline = { 
       \ 'colorscheme': 'selenized_dark',
       \ }

let g:lightline = { 
       \ 'colorscheme': 'jellybeans',
       \ 'active': {
       \   'left': [ ['mode', 'paste'],
       \             ['fugitive', 'readonly', 'filename', 'modified'] ],
       \   'right': [ [ 'lineinfo' ], ['fileencoding','filetype','percent'] ]
       \ },
       \ 'component': {
       \   'readonly': '%{&filetype=="help"?"":&readonly?"\ue0a2":""}',
       \   'modified': '%{&filetype=="help"?"":&modified?"\ue0a0":&modifiable?"":"-"}',
       \   'fugitive': '%{exists("*fugitive#head")?fugitive#head():""}'
       \ },
       \ 'component_visible_condition': {
       \   'readonly': '(&filetype!="help"&& &readonly)',
       \   'modified': '(&filetype!="help"&&(&modified||!&modifiable))',
       \   'fugitive': '(exists("*fugitive#head") && ""!=fugitive#head())'
       \ },
       \ 'separator': { 'left': "\ue0b0", 'right': "\ue0b2" },
       \ 'subseparator': { 'left': "\ue0b1", 'right': "\ue0b3" }
       \ }   ""

" Rust
autocmd BufWritePost *.rs :silent !cargo fmt <afil
let g:rustfmt_autosave = 1
let g:rustfmt_emit_files = 1
let g:rustfmt_fail_silently = 0
let g:rust_clip_command = 'xclip -selection clipboard'

" LaTeX
autocmd BufEnter *.tex :setlocal filetype=tex
autocmd BufWritePost *.tex :silent !xelatex <afile>

" Language server
lua << EOF
local nvim_lsp = require'lspconfig'

local on_attach = function(client)
    require'completion'.on_attach(client)
end

nvim_lsp.rust_analyzer.setup({
    on_attach=on_attach,
    settings = {
        ["rust-analyzer"] = {
            assist = {
                importGranularity = "module",
                importPrefix = "by_self",
            },
            cargo = {
                loadOutDirsFromCheck = true
            },
            procMacro = {
                enable = true
            },
        }
    }
})
EOF
