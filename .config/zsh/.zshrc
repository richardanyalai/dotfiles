#!/bin/sh
#
#    ████████ ██                               ██    
#   ██░░░░░░ ░██                              ░██    
#  ░██       ░██      ██████  ██████  ███████ ░██  ██
#  ░█████████░██████ ░░██░░█ ██░░░░██░░██░░░██░██ ██ 
#  ░░░░░░░░██░██░░░██ ░██ ░ ░██   ░██ ░██  ░██░████  
#         ░██░██  ░██ ░██   ░██   ░██ ░██  ░██░██░██ 
#   ████████ ░██  ░██░███   ░░██████  ███  ░██░██░░██
#  ░░░░░░░░  ░░   ░░ ░░░     ░░░░░░  ░░░   ░░ ░░  ░░

# Enable Vi mode
set -o vi

# Enable colors and change prompt:
#export TERM="xterm-256color"
autoload -U colors && colors

# Custom prompt text
unsetopt PROMPT_SP
#PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$fg[green]%}$%{$reset_color%}%b "

ZSH_THEME="robbyrussell"
source ${ZDOTDIR}/oh-my-zsh/oh-my-zsh.sh

# Go to a directory by typing its name
setopt auto_cd

# History in cache directory:
HISTFILE=${ZDOTDIR}/histfile
HISTSIZE=1000
SAVEHIST=1000

setopt appendhistory
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS
setopt HIST_REDUCE_BLANKS
setopt HIST_VERIFY

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
# Include hidden files
_comp_options+=(globdots)

# Make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# Set a fancy prompt (non-color, unless we know we "want" color)
case "${TERM}" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# Enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# Some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Bind keys to search in command history
bindkey "^[[A"	history-beginning-search-backward
bindkey "^[[B"	history-beginning-search-forward

# Fix Home, End and Delete key
bindkey "^[[2~" quoted-insert			# Insert
bindkey "^[[3~"	delete-char				# Delete
bindkey "^[[5~" beginning-of-history	# PageUp
bindkey "^[[6~" end-of-history			# PageDown
bindkey "^[[7~" beginning-of-line		# Home
bindkey "^[[8~" end-of-line				# End

# But.... home and end key escape sequences 
# are DIFFERENT depending on whether I'm in a tmux session or not!
# To determine if tmux is running, examine values of $TERM and $TMUX.
if [ "${TERM}" = "xterm-256color" ] && [ -n "${TMUX}" ]; then
  bindkey "^[[1~" beginning-of-line
  bindkey "^[[4~" end-of-line
else
  # Assign these keys if tmux is NOT being used:
  bindkey "^[[H" beginning-of-line
  bindkey "^[[F" end-of-line
fi

# Vi mode
bindkey -v
export KEYTIMEOUT=1

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# Change cursor shape for different vi modes.
function zle-keymap-select {
    if [[ ${KEYMAP} = vicmd ]] ||
       [[ ${1} = 'block' ]]; then
        echo -ne '\e[1 q'
    elif [[ ${KEYMAP} = main ]] ||
       [[ ${KEYMAP} = viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ ${1} = 'beam' ]]; then
        echo -ne '\e[5 q'
    fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' } # Use beam shape cursor for each new prompt.

# Add an "alert" alias for long running commands.  Use like so:
# sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Load environmental variables
source ${ZDOTDIR}/envvars

# Load my shit ton of aliases
source ${ZDOTDIR}/aliasrc

# Load zsh-syntax-highlighting; should be last.
source ${ZDOTDIR}/zsh-autosuggestions/zsh-autosuggestions.zsh
source ${ZDOTDIR}/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

