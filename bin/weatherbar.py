import requests
import sys
import urllib

openWeatherMap_api_key = 'dbfa0741d3afadbf3e67541f3e920496' # openweather api key
openWeatherMap_api = 'https://api.openweathermap.org/data/2.5/weather?q=' # openweather api URL

try:
	city = ' '.join(sys.argv[1:])
	url = openWeatherMap_api + city + '&appid=' + openWeatherMap_api_key + '&units=metric&lang=en'
	weather = requests.get(url).json()

	desc = weather['weather'][0]['description']

	status = desc[0].title() + desc[1:].lower()

	icon = ''

	if 'storm' in status:
		icon = ''
	elif 'shower' in status or 'rain' in status:
		icon = ''
	elif 'cloud' in status:
		icon = ''
	elif 'snow' in status:
		icon = ''

	weather_data = f" {icon} {str(weather['main']['temp'])}°C 煮 {str(weather['wind']['speed'])} km/h"

	print(weather_data)
except Exception as e:
	print()
